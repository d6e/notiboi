extern crate reqwest;
use actix_web::client::Client;
use actix_web::web;
use actix_web::App;
use actix_web::HttpResponse;
use actix_web::HttpServer;
use actix_web::Responder;
use std::thread;
use std::time::Duration;
use std::time::SystemTime;
use std::time::UNIX_EPOCH;

fn index() -> impl Responder {
    HttpResponse::Ok().body("Hello world!")
}

fn cron_trigger() -> impl Responder {
    HttpResponse::Ok().body("success.")
}

fn main() {
    let uri = "127.0.0.1:8088";
    let client = reqwest::Client::new();
    thread::spawn(move || loop {
        let resp = client
            .post("http://127.0.0.1:8088/cron/trigger")
            .send()
            .and_then(|mut x| x.text());
        println!("{:?}", resp);
        // let timestamp = SystemTime::now().duration_since(UNIX_EPOCH).unwrap();
        // println!("{}", timestamp.as_secs());
        thread::sleep(Duration::from_millis(1000));
    });
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(index))
            .route("/cron/trigger", web::post().to(cron_trigger))
    })
    .bind(uri)
    .unwrap()
    .run()
    .unwrap();
}
