# Contributing
1. Install pre-commit:
```
pip install pre-commit
```
2. Install the pre-commit scripts:
```
pre-commit install
```

# Categories of notifications (priority order):
1. Call
2. Text
3. Message (short)
4. Message (long)
5. Email


# Mediums:
- Phone
  * call
  * text
- Message (long)
  * slack
  * discord
- Message (short)
  * signal
  *

# Things to do:
## Async Database Access:
- https://brandur.org/rust-web#sync-actors

## Database
- Diesel https://actix.rs/docs/databases/

## HTML Templating
- https://lib.rs/crates/askama
